if exists("b:did_indent")
   finish
endif
let b:did_indent = 1

setlocal indentexpr=GetPropertiesIndent()

if exists("*GetPropertiesIndent")
 finish
endif

" Adapted GetJSONIndent()
function GetPropertiesIndent()
  let vcol = col('.')
  let line = getline(v:lnum)
  let col = matchend(line, '^\s*}')
  if col > 0
    call cursor(v:lnum, col)
    let pairline = searchpair('{', '', '}', 'bW')
    if pairline > 0 
      let ind = indent(pairline)
    else
      let ind = virtcol('.') - 1
    endif
    return ind
  endif
  let lnum = prevnonblank(v:lnum - 1)
  if lnum == 0
    return 0
  endif
  let line = getline(lnum)
  let ind = indent(lnum)
  if line =~ '{'
    return ind + &sw
  endif
  return ind
endfunction
