if exists("b:current_syntax")
  finish
endif

syntax keyword propertiesType string int uint long ulong double numeric boolean bool pointer properties

syntax keyword propertiesBoolean true false

syn match propertiesComment "#.*$"

" Adapted from JSON
syn match propertiesNumber  "\(= *\)\@<=-\=\<\%(0\|[1-9]\d*\)\%(\.\d\+\)\=\%([eE][-+]\=\d\+\)\=\>\ze[[:blank:]\r\n]*[,}\]]"
syn match propertiesString "\(= *\)\@<=[^{]\+$" contains=propertiesComment

syn region propertiesNested matchgroup=propertiesBraces start="{" end="}" fold transparent

hi def link propertiesType Type
hi def link propertiesBoolean Boolean
hi def link propertiesComment Comment
hi def link propertiesNumber Number
hi def link propertiesString String
