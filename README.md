# properties.vim

Vim plugin for [MCLabUtils](https://bitbucket.org/mclab/mclabutils) Properties files.

## Features

When you edit `*.prop.txt` you get:

* syntax highlighting
* autoindentation
* folding of nested Properties when using `foldmethod=syntax`
* `commentstring` - so that plugins like [vim-commentary](https://github.com/tpope/vim-commentary) work as intended

![Demo](demo.png)
